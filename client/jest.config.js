module.exports = {
  moduleNameMapper: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js',
    '\\.(s?css)$': 'identity-obj-proxy',
  },
  setupFilesAfterEnv: ['jest-enzyme', 'babel-polyfill'],
  testEnvironment: 'enzyme',
  testEnvironmentOptions: {
    enzymeAdapter: 'react16',
  },
  testMatch: ['**/*.test.js'],
  verbose: true,
};
