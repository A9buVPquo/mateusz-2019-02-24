import eventEmitter from 'event-emitter';

const eventsBus = eventEmitter();

export { eventsBus };
