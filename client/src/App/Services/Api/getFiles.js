async function getFiles() {
  return fetch('/api/files', {
    method: 'GET',
  })
    .then((response) => {
      if (response.status >= 200 && response.status < 400) {
        return response.json();
      }

      throw new Error('Unable to retrieve files');
    });
}

export { getFiles };
