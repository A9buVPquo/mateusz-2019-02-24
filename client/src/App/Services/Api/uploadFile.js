async function uploadFile(formData) {
  return fetch('/api/files', {
    method: 'POST',
    body: formData,
  })
    .then((response) => {
      if (response.status !== 200) {
        throw new Error('Can not upload a file!');
      }

      return response.json();
    });
}

export { uploadFile };
