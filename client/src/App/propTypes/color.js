import PropTypes from 'prop-types';

export const COLOR_ACCENT = 'accent';
export const COLOR_PRIMARY = 'primary';
export const COLOR_SECONDARY = 'secondary';

export const color = PropTypes.oneOf([
  COLOR_ACCENT,
  COLOR_PRIMARY,
  COLOR_SECONDARY,
]);
