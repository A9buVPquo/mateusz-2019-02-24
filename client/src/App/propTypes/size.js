import PropTypes from 'prop-types';

export const SIZE_SMALL = 'small';
export const SIZE_MEDIUM = 'medium';
export const SIZE_LARGE = 'large';

export const size = PropTypes.oneOf([
  SIZE_LARGE,
  SIZE_MEDIUM,
  SIZE_SMALL,
]);
