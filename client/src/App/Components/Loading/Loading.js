import React from 'react';

import styles from './Loading.scss';

const Loading = () => {
  return (
    <div className={ styles.container }>
      <div className={ styles.element1 } />
      <div className={ styles.element2 } />
      <div className={ styles.element3 } />
      <div className={ styles.element4 } />
    </div>
  );
};

export { Loading };
