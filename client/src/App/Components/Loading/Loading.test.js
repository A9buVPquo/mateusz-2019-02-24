import { shallow } from 'enzyme';
import React from 'react';

import { Loading } from './Loading';

const defaultProps = {};

const render = (props = defaultProps) => {
  return shallow(<Loading {...props} />);
}

describe('<Loading />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });
});
