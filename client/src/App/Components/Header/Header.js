import classnames from 'classnames';
import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { FileUpload } from '../FileUpload/FileUpload';
import { InputText } from '../InputText/InputText';
import styles from './Header.scss';

class Header extends Component {
  static propTypes = {
    className: PropTypes.string,
    onSearchChange: PropTypes.func,
  };

  static defaultProps = {
    className: '',
    onSearchChange: noop,
  };

  constructor(props) {
    super(props);

    this.state = {
      search: '',
    };

    this.onSearchChange = this.onSearchChange.bind(this);
  }

  onSearchChange(event) {
    const { target: { value } } = event;

    this.setState({ search: value }, () => {
      this.props.onSearchChange(this.state.search);
    });
  }

  render() {
    const { className } = this.props;
    const { search } = this.state;

    return (
      <div className={ classnames(styles.container, className) }>
        <InputText
          className={ styles.searchForm }
          onChange={ this.onSearchChange }
          placeholder="Search"
          value={ search }
        />
        <FileUpload
          className={ styles.uploadButton }
        />
      </div>
    );
  }
}

export { Header };
