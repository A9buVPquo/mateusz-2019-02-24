import { shallow } from 'enzyme';
import React from 'react';

import { FileUpload } from '../FileUpload/FileUpload';
import { Header } from './Header';
import { InputText } from '../InputText/InputText';

const defaultProps = {};

const render = (props = defaultProps) => {
  return shallow(<Header { ...props } />);
}

describe('<Header />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });

  it('should render upload form', () => {
    // when
    const wrapper = render();

    // then
    expect(wrapper.find(FileUpload)).toHaveLength(1);
  });

  it('should render search input', () => {
    // when
    const wrapper = render();

    // then
    expect(wrapper.find(InputText)).toHaveLength(1);
  });

  it('should add custom class', () => {
    // when
    const wrapper = render({ className: 'custom-class' });

    // then
    expect(wrapper.hasClass('custom-class')).toBe(true);
  });
});
