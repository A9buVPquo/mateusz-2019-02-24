import classnames from 'classnames';
import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

import styles from './InputText.scss';

const InputText = ({
  className,
  onChange,
  placeholder,
  value,
}) => {
  return (
    <input type="text"
      className={ classnames(styles.input, className) }
      onChange={ onChange }
      placeholder={ placeholder }
      value={ value }
    />
  );
};

InputText.propTypes = {
  className: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

InputText.defaultProps = {
  onChange: noop,
  value: '',
};

export { InputText };
