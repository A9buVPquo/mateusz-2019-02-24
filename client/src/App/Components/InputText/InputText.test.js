import { shallow } from 'enzyme';
import React from 'react';

import { Button } from '../Button/Button';
import { InputText } from './InputText';

const defaultProps = {};

const render = (props = defaultProps) => {
  return shallow(<InputText {...props} />);
}

describe('<InputText />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });

  it('should render input', () => {
    // when
    const wrapper = render();

    // then
    expect(wrapper.find('input[type="text"]')).toHaveLength(1);
  });

  it('should add proper class', () => {
    // when
    const wrapper = render({ className: 'custom-class' });

    // then
    expect(wrapper.hasClass('custom-class')).toBe(true);
  });

  it('should add placeholder', () => {
    // when
    const wrapper = render({ placeholder: 'Search' });

    // then
    expect(wrapper.html()).toMatchSnapshot();
  });

  it('should call on change', () => {
    // having
    const onChange = jest.fn();

    // when
    const wrapper = render({ onChange });
    wrapper.simulate('change');

    // then
    expect(onChange).toHaveBeenCalled();
  });

  it('should display value', () => {
    // when
    const wrapper = render({ placeholder: 'javascript' });

    // then
    expect(wrapper.html()).toMatchSnapshot();
  });
});
