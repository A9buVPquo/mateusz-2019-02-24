import { shallow } from 'enzyme';
import React from 'react';

import { FileUpload } from './FileUpload';

const defaultProps = {};

const render = (props = defaultProps) => {
  return shallow(<FileUpload {...props} />);
}

describe('<FileUpload />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });

  it('should render given className', () => {
    // when
    const wrapper = render({ className: 'custom-class' });

    // then
    expect(wrapper.hasClass('custom-class')).toBe(true);
  });
});
