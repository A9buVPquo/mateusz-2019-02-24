import classnames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { uploadFile } from '../../Services/Api/uploadFile';
import { FILE_UPLOADED } from '../../Services/EventsBus/events';
import { eventsBus } from '../../Services/EventsBus/eventsBus';
import { Button } from '../Button/Button';
import styles from './FileUpload.scss';

class FileUpload extends Component {
  static propTypes = {
    className: PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.state = {
      files: [],
      isBeingUploaded: false,
      isUploaded: false,
      hasError: false,
    };

    this._file = React.createRef();

    this.handleButtonClick = this.handleButtonClick.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
  }

  handleButtonClick() {
    const { isBeingUploaded, isUploaded, hasError } = this.state;

    if (!this._file.current || isBeingUploaded || isUploaded || hasError) {
      return;
    }

    this._file.current.click();
  }

  handleFileChange(event) {
    const { target } = event;
    const [file] = target.files;

    const formData = new FormData();

    formData.append('file', file);

    this.setState({
      isBeingUploaded: true,
      isUploaded: false,
    }, async () => {
      try {
        const { fileData } = await uploadFile(formData);

        this.setState({
          isBeingUploaded: false,
          isUploaded: true,
        }, () => {
          this.resetState();
          eventsBus.emit(FILE_UPLOADED, fileData);
        });
      } catch (err) {
        this.setState({
          isBeingUploaded: false,
          hasError: true,
        }, this.resetState);
      }

      this._file.current.value = '';
    });
  }

  resetState() {
    setTimeout(() => {
      this.setState({
        isBeingUploaded: false,
        isUploaded: false,
        hasError: false,
      });
    }, 3000);
  }

  renderMessage() {
    const { isBeingUploaded, isUploaded, hasError } = this.state;

    if (isBeingUploaded) {
      return <div className={ styles.message }>Please wait...</div>;
    }

    if (isUploaded) {
      return (
        <div
          className={ classnames(styles.message, styles.messageSuccess) }
        >
          File has been uploaded!
        </div>
      );
    }

    if (hasError) {
      return (
        <div
          className={ classnames(styles.message, styles.messageDanger) }
        >
          There was an error, please try again!
        </div>
      );
    }

    return null;
  }

  render() {
    const { className } = this.props;
    const { isBeingUploaded } = this.state;

    return (
      <div className={ classnames(styles.container, className) }>
        { this.renderMessage() }
        <input
          type="file"
          className={ styles.fileInput }
          accept="image/x-png,image/jpeg"
          ref={ this._file }
          onChange={ this.handleFileChange }
        />
        <Button
          className={ styles.button }
          isDisabled={ isBeingUploaded }
          onClick={ this.handleButtonClick }
        >
          Upload
        </Button>
      </div>
    );
  }
}

export { FileUpload };
