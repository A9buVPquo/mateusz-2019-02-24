import React, { Component } from 'react';

import { getFiles } from '../../Services/Api/getFiles';
import { FILE_UPLOADED } from '../../Services/EventsBus/events';
import { eventsBus } from '../../Services/EventsBus/eventsBus';
import { FilesContainer } from '../Files/FilesContainer';
import { Header } from '../Header/Header';
import styles from './App.scss';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      files: [],
      filesCount: 0,
      filesSize: 0,
      isFilesBeingLoaded: false,
      isFilesLoaded: false,
    };

    this.handleFileUploaded = this.handleFileUploaded.bind(this);
  }

  componentDidMount() {
    eventsBus.on(FILE_UPLOADED, this.handleFileUploaded);
    this.getFiles();
  }

  componentWillUnmount() {
    eventsBus.off(FILE_UPLOADED, this.handleFileUploaded);
  }

  handleFileUploaded() {
    this.getFiles();
  }

  getFiles() {
    this.setState({
      isFilesBeingLoaded: true,
    }, async () => {
      try {
        const { collection, meta } = await getFiles();

        this.setState({
          files: collection,
          filesCount: meta.count,
          filesSize: meta.size,
          isFilesBeingLoaded: false,
          isFilesLoaded: true,
        });
      } catch (e) {
        this.setState({
          isFilesBeingLoaded: false,
          hasError: true,
        });
      }
    });
  }

  render() {
    const { files, filesCount, filesSize, isFilesBeingLoaded, isFilesLoaded } = this.state;

    return (
      <div className={ styles.container }>
        <Header className={ styles.header } />
        <FilesContainer
          files={ files }
          filesCount={ filesCount }
          filesSize={ filesSize }
          isBeingLoaded={ isFilesBeingLoaded }
          isLoaded={ isFilesLoaded }
        />
      </div>
    );
  }
}

export { App };
