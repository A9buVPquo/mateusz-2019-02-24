import { shallow } from 'enzyme';
import React from 'react';

import { App } from './App';

const defaultProps = {};

const render = (props = defaultProps) => {
  return shallow(<App {...props} />);
}

describe('<App />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });
});
