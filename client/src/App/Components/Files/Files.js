import PropTypes from 'prop-types';
import React, { Component } from 'react';

import { Loading } from '../Loading/Loading';
import { File } from './File';
import styles from './Files.scss';
import { FilesMeta } from './FilesMeta';

class Files extends Component {
  static propTypes = {
    files: PropTypes.array.isRequired,
    filesCount: PropTypes.number.isRequired,
    filesSize: PropTypes.number.isRequired,
    isBeingLoaded: PropTypes.bool.isRequired,
  };

  renderFiles() {
    const { files } = this.props;

    return (
      <div className={ styles.filesContainer }>
        { files.map((file) => <File key={ file.id } data={ file } />) }
      </div>
    );
  }

  render() {
    const { filesCount, filesSize, isBeingLoaded } = this.props;

    return (
      <div className={ styles.container }>
        <FilesMeta count={ filesCount } size={ filesSize } />
        { this.renderFiles() }
        { isBeingLoaded ? <Loading /> : null }
      </div>
    );
  }
}

export { Files };
