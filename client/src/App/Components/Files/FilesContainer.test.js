import { shallow } from 'enzyme';
import React from 'react';

import { Loading } from '../Loading/Loading';
import { Files } from './Files';
import { FilesContainer } from './FilesContainer';

const defaultProps = {
  files: [],
  filesCount: 0,
  filesSize: 0,
  isBeingLoaded: false,
  isLoaded: false,
};

const render = (props = defaultProps) => {
  return shallow(<FilesContainer {...props} />);
}

describe('<FilesContainer />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });

  describe('isBeingLoaded', () => {
    it('should display only loading indicator on initial files load', () => {
      // when
      const wrapper = render({ isBeingLoaded: true, isLoaded: false });

      // then
      expect(wrapper.find(Loading)).toHaveLength(1);
      expect(wrapper.find(Files)).toHaveLength(0);
    });
  })

  describe('isLoaded', () => {
    it('should display files when loaded', () => {
      // when
      const wrapper = render({ isBeingLoaded: false, isLoaded: true });

      // then
      expect(wrapper.find(Loading)).toHaveLength(0);
      expect(wrapper.find(Files)).toHaveLength(1);
    });
  });
});
