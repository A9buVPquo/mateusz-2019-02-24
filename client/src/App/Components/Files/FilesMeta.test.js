import { shallow } from 'enzyme';
import React from 'react';

import { FilesMeta } from './FilesMeta';
import { FileSize } from './FileSize';

const defaultProps = {
  count: 0,
  size: 0,
};

const render = (props = defaultProps) => {
  return shallow(<FilesMeta {...props} />);
}

describe('<FilesMeta />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });

  it('should render given metadata', () => {
    // when
    const wrapper = render({ count: 10 });

    // then
    expect(wrapper.contains('10 documents'));
    expect(wrapper.find(FileSize)).toHaveLength(1);
  });
});
