import { PropTypes } from 'prop-types';
import React, { Component } from 'react';

import { Loading } from '../Loading/Loading';
import { Files } from './Files';
import styles from './FilesContainer.scss';

class FilesContainer extends Component {
  static propTypes = {
    files: PropTypes.array.isRequired,
    filesCount: PropTypes.number.isRequired,
    filesSize: PropTypes.number.isRequired,
    isBeingLoaded: PropTypes.bool.isRequired,
    isLoaded: PropTypes.bool.isRequired,
  };

  renderInitialLoading() {
    const { isBeingLoaded, isLoaded } = this.props;

    return isBeingLoaded && !isLoaded
      ? <Loading />
      : null;
  }

  renderFiles() {
    const { files, filesCount, filesSize, isBeingLoaded, isLoaded } = this.props;

    return isLoaded
      ? (
        <Files
          files={ files }
          filesCount={ filesCount }
          filesSize={ filesSize }
          isBeingLoaded={ isBeingLoaded }
        />
      )
      : null;
  }

  render() {
    return (
      <div className={ styles.container }>
        { this.renderInitialLoading() }
        { this.renderFiles() }
      </div>
    );
  }
}

export { FilesContainer };
