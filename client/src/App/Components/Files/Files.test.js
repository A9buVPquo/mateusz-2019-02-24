import { shallow } from 'enzyme';
import React from 'react';

import { Files } from './Files';

const defaultProps = {
  files: [],
  filesCount: 0,
  filesSize: 0,
  isBeingLoaded: false,
};

const render = (props = defaultProps) => {
  return shallow(<Files {...props} />);
}

describe('<Files />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });
});
