import PropTypes from 'prop-types';
import React from 'react';

import styles from './File.scss';
import { FileSize } from './FileSize';

const File = ({ data }) => {
  return (
    <div className={ styles.container }>
      <div className={ styles.fileName }>{ data.originalFilename }</div>
      <div className={ styles.fileSize }><FileSize size={ data.size } /></div>
    </div>
  );
};

File.propTypes = {
  data: PropTypes.object.isRequired,
};

export { File };
