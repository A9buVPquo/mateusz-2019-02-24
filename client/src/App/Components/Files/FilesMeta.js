import PropTypes from 'prop-types';
import React from 'react';

import styles from './FilesMeta.scss';
import { FileSize } from './FileSize';

const FilesMeta = ({ count, size }) => {
  return (
    <div className={ styles.container }>
      <div className={ styles.count }>{ count } documents</div>
      <div>total size: <FileSize size={ size } /></div>
    </div>
  );
};

FilesMeta.propTypes = {
  count: PropTypes.number.isRequired,
  size: PropTypes.number.isRequired,
};

export { FilesMeta };
