import filesize from 'filesize';
import PropTypes from 'prop-types';
import React from 'react';

const FileSize = ({ size }) => {
  return <span>{ filesize(size) }</span>;
};

FileSize.propTypes = {
  size: PropTypes.number.isRequired,
};

export { FileSize };
