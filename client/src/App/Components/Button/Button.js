import classnames from 'classnames';
import { noop } from 'lodash';
import PropTypes from 'prop-types';
import React from 'react';

import { color, COLOR_PRIMARY } from '../../propTypes/color';
import { size, SIZE_MEDIUM } from '../../propTypes/size';
import styles from './Button.scss';

const Button = ({ children, className, color, isDisabled, onClick, size }) => {
  const buttonClasses = classnames(
    styles.button,
    styles[color],
    styles[size],
    className,
    {
      [styles.disabled]: isDisabled,
    },
  );

  return (
    <button
      className={ buttonClasses }
      disabled={ isDisabled }
      onClick={ onClick }
    >
      { children }
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  color,
  isDisabled: PropTypes.bool,
  onClick: PropTypes.func,
  size,
};

Button.defaultProps = {
  className: '',
  color: COLOR_PRIMARY,
  onClick: noop,
  size: SIZE_MEDIUM,
};

export { Button };
