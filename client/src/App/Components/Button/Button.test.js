import { shallow } from 'enzyme';
import React from 'react';
import { Button } from './Button';

const defaultProps = { children: 'Test' };

const render = ({ children, ...props } = defaultProps) => {
  return shallow(
    <Button { ...props }>{ children }</Button>
  );
}

describe('<Button />', () => {
  it('should render without error', () => {
    // having
    console.error = jest.fn();

    // when
    render();

    // then
    expect(console.error).not.toBeCalled();
    console.error.mockRestore();
  });

  it('should render with given text', () => {
    // when
    const wrapper = render({ children: 'Login' });

    // then
    expect(wrapper.contains('Login')).toBe(true);
  });

  it('should call passed onClick function when clicked', () => {
    // having
    const onClick = jest.fn();

    // when
    const wrapper = render({ onClick });
    wrapper.simulate('click');

    // then
    expect(onClick).toHaveBeenCalled();
  });

  it('should apply passed className', () => {
    // when
    const wrapper = render({ className: 'custom-class' });

    // then
    expect(wrapper.hasClass('custom-class')).toBe(true);
  });

  describe('color', () => {
    it('should apply default color class', () => {
      // when
      const wrapper = render();

      // then
      expect(wrapper.hasClass('primary')).toBe(true);
    });

    it('should apply given color class', () => {
      // when
      const wrapper = render({ color: 'secondary' });

      // then
      expect(wrapper.hasClass('secondary')).toBe(true);
    });
  });

  describe('size', () => {
    it('should apply default size class', () => {
      // when
      const wrapper = render();

      // then
      expect(wrapper.hasClass('medium')).toBe(true);
    });

    it('should apply given size class', () => {
      // when
      const wrapper = render({ size: 'large' });

      // then
      expect(wrapper.hasClass('large')).toBe(true);
    });
  });

  describe('disabled', () => {
    let wrapper;

    beforeEach(() => {
      // when
      wrapper = render({ isDisabled: true });
    });

    it('should make button disabled', () => {
      // then
      expect(wrapper.find('button[disabled]')).toHaveLength(1);
    });

    it('should add disabled class', () => {
      // then
      expect(wrapper.hasClass('disabled')).toBe(true);
    });
  });
});
