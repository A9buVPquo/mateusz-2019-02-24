import PropTypes from 'prop-types';
import React from 'react';

import styles from './AppContainer.scss';

const AppContainer = ({ children }) => {
  return (
    <div className={ styles.container }>
      { children }
    </div>
  );
};

AppContainer.propTypes = {
  children: PropTypes.node,
};

export { AppContainer };
