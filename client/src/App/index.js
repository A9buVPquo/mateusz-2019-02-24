import '../styles/styles.scss';

import 'whatwg-fetch';
import React from 'react';
import ReactDOM from 'react-dom';

import { App } from './Components/App/App';
import { AppContainer } from './Components/AppContainer/AppContainer';

ReactDOM.render(
  <AppContainer>
    <App />
  </AppContainer>,
  document.getElementById('root'),
);
