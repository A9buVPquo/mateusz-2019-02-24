const path = require('path');
const dist = path.join(__dirname, '../dist');

module.exports = {
  paths: {
    dist,
    baseUrl: '/',
  },
  ports: {
    backend: 3000,
    frontend: 3001,
  },
};
