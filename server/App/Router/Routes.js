const Joi = require('joi');

const fileUploadHandler = require('../Modules/Api/fileUploadHandler');
const getFilesHandler = require('../Modules/Api/getFilesHandler');

const routes = [
  {
    method: 'GET',
    path: '/api/files',
    handler: getFilesHandler,
  },
  {
    method: 'POST',
    path: '/api/files',
    handler: fileUploadHandler,
    config: {
      payload: {
        allow: 'multipart/form-data',
        maxBytes: 1024 * 1024 * 10,
        parse: true,
        output: 'stream',
      },
      validate: {
        payload: Joi.object({
          file: Joi.any(),
        }),
      },
    },
  },
];

module.exports = {
  routes,
};
