function initRoutes(server, routes) {
  routes.forEach((route) => server.route(route));
}

module.exports = {
  initRoutes,
};
