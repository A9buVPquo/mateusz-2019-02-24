const Boom = require('boom');
const fs = require('fs');
const fsExtra = require('fs-extra');
const path = require('path');
const uuid = require('uuid');

const getFileData = require('./getFileData');

const STORAGE_PATH = path.resolve(__dirname, '../../../storage');
const DB_PATH = path.resolve(__dirname, '../../../db/files.json');

const defaultOptions = {
  allowedMimetypes: [
    'image/png',
    'image/jpg',
  ],
  path: STORAGE_PATH,
};

function upload(file, options = defaultOptions) {
  if (!options.allowedMimetypes.includes(file.hapi.headers['content-type'])) {
    throw new Error('Forbidden file type!');
  }

  const filename = file.hapi.filename;
  const extension = getExtension(filename);
  const id = uuid.v4();
  const filepath = path.resolve(options.path, `${id}${extension}`);
  const stream = fs.createWriteStream(filepath);

  return new Promise((resolve, reject) => {
    file.on('error', (err) => reject(err));

    file.pipe(stream);

    file.on('end', () => {
      const stats = fs.statSync(filepath);

      resolve({
        filepath,
        filename: `${id}${extension}`,
        originalFilename: file.hapi.filename,
        id,
        size: stats.size,
        createdAt: Date.now(),
      });
    });
  });
}

async function saveFileToDatabase(fileData) {
  let files = {};

  try {
    files = await fsExtra.readJson(DB_PATH);

    files.collection = [
      fileData,
      ...files.collection,
    ];
  } catch (err) {
    files.collection = [fileData];
  }

  await fsExtra.writeJson(DB_PATH, files);
}

async function fileUploadHandler(request) {
  try {
    const { file } = request.payload;

    const fileData = await upload(file);

    await saveFileToDatabase(fileData);

    return {
      status: 1,
      fileData: getFileData(fileData),
    };
  } catch (err) {
    return Boom.badRequest(err.message, err);
  }
}

function getExtension(filename) {
  return filename.substring(filename.lastIndexOf('.'));
}

module.exports = fileUploadHandler;
