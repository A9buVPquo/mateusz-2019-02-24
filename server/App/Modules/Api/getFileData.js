function getFileData(file) {
  const { createdAt, filename, id, originalFilename, size } = file;

  return {
    createdAt,
    filename,
    id,
    originalFilename,
    size,
    url: `/file/${filename}`,
  };
}

module.exports = getFileData;
