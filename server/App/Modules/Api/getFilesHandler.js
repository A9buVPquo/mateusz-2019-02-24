const fsExtra = require('fs-extra');
const path = require('path');

const getFileData = require('./getFileData');

const DB_PATH = path.resolve(__dirname, '../../../db/files.json');

async function getFilesHandler() {
  try {
    const files = await fsExtra.readJson(DB_PATH);
    const meta = {
      count: files.collection.length,
      size: 0,
    };

    const collection = files.collection.map((file) => {
      meta.size += file.size;

      return getFileData(file);
    });

    return {
      collection,
      meta,
    };
  } catch (err) {
    return {
      collection: [],
      meta: {
        count: 0,
        size: 0,
      },
    };
  }
}

module.exports = getFilesHandler;
