'use strict';

const Hapi = require('hapi');

const { initRoutes } = require('./App/Router/initRoutes');
const { routes } = require('./App/Router/Routes');

const server = Hapi.server({
  port: 3000,
  host: 'localhost',
});

initRoutes(server, routes);

const init = async () => {
  await server.start();
  console.log(`Server running at: ${server.info.uri}`);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
