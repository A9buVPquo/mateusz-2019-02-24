# Mateusz - 2019-02-24

## Installation
### Prerequisites
1. `node >= 8`
2. Available ports `3000` and `3001` (for client and server).

### Installation process
```
cd ./client && yarn install
cd ../server && yarn install
```

## Running
1. Please run server and client in separate terminals:
```
cd client && yarn run start
```
```
cd server && yarn run start
```
2. Open browser at `http://localhost:3001/`.

## Tests
### Client
`yarn run test`

## Security
### Addressed concerns
- backend & frontend validation of file type (based on mime),
- backend validation of file size,

### Not addressed concerns
- frontend validation of file size,
- don't store the files on the same machine that the application - ideally files should be piped and saved outside, e.g. in Amazon S3

## Improvements
### App
- search - I ran out of time and didn't implement search feature :(,
- integration and E2E tests,
- pagination (might be implemented using normal pagination controls, using infinite scroll or via load button at the bottom),
- displaying thumbnails of the images,
- enhanced notification system (e.g. using some toasts),
- upload via drag & drop,
- upload multiple files,
- search autocompletion.

### API
- unit tests,
- pagination,
- rate limiting,
- mimetype validation done in route declaration via Joi (not sure if it's possible - should be double checked)
- data storage (I used simple JSON file as I treat this API as a mock).

## Libraries
I described only production dependencies - if I describe also dev dependencies just let me know and I will elaborate more on my choices.

### Client
- `classnames` - handy library to elements classNames management
- `event-emitter` - environment agnostic event-emitter to introduce a way to share events across the application
- `lodash` - utilities
- `normalize.css` - consistent styles across all browsers - resets all default settings
- `prop-types` - React prop-types
- `react`
- `react-dom`
- `whatwg-fetch` - fetch polyfill

### Server
- `boom` - handling errors in hapi.js
- `fs-extra` - promise based filesystem methods
- `hapi` - application framework
- `joi` - validation tool
- `uuid` - uuid generator

## API
Don't want to justify myself, but I treat an API more as a mock - it is not first class implementation, however I tried to make it the best and it's not as bad assuming the time I spent on it :)

### Get all files
*Method:* `GET`
*URL:* `/api/files`
*Auth:* no

#### Success respoinse
*Code:* `200 OK`
```json
{
  "collection": [
    {
      "createdAt": 1551034146575,
      "filename": "47cf2d1d-24d2-45db-b2ea-b1800e0909c2.png",
      "id": "47cf2d1d-24d2-45db-b2ea-b1800e0909c2",
      "originalFilename": "3.png",
      "size": 30395,
      "url": "/file/47cf2d1d-24d2-45db-b2ea-b1800e0909c2.png"
    },
    // ...
  ],
  "meta": {
    "count": 10,
    "size": 203940294
  }
}
```
- returns JSON representation of files collection,
- additionally returns stats needed to be displayed on the frontend side, like size sum and number of documents,
- it should also accept pagination and filtering (e.g. by name) parameters, but I ran out of time.

### Upload a file
*Method:* `POST`
*URL:* `/api/files`
*Auth:* no
*Content-Type:* `multipart/form-data`

#### Request payload
```json
{
  "file": <uploaded_file_data>
}
```

#### Success response
*Code:* `200 OK`
```json
{
  "fileData": {
    "createdAt": 1551035812974,
    "filename": "49187386-2f15-405c-a31e-7f1fff4fc146.png",
    "id": "49187386-2f15-405c-a31e-7f1fff4fc146",
    "originalFilename": "3.png",
    "size": 16384,
    "url": "/file/49187386-2f15-405c-a31e-7f1fff4fc146.png"
  },
  "status": 1
}
```
---
## Other notes
The test was interesting - I have to say that development without external state management is... different :) It requires developer to change the mindset a little bit.

From the other side - I didn't like that I lost a lot of time on bootstrapping the application - setting up webpack, servers, etc. I don't mind doing it, but if I have had a small tip about necessary environment I would probably prepare it earlier and spent all the time coding. It would be nice to have such information when we scheduled the test.
